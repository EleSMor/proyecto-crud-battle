const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const classSchema = new Schema(
    {
        name: { type: String, required: true},
        classAttributes: {
            class_attack: { type: Number, required: true, default: 0 },
            class_defense: { type: Number, required: true, default: 0 },
            class_healthPoints: { type: Number, required: true, default: 0 },
            class_criticalRate: { type: Number, required: true, default: 0 },
            class_criticalDamage: { type: Number, required: true, default: 0 }
        },
        characters: [{ type: mongoose.Types.ObjectId, ref: 'Characters' }],
    },
    { timestamp: true }
);

const Class = mongoose.model('Classes', classSchema);

module.exports = Class;