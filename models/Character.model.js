const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const characterSchema = new Schema(
    {
        name: { type: String, required: true },
        level: { type: Number, required: true, min: 0, default: 0 },
        characterClass: { type: String, required: true },
        race: { type: String, required: true },
        attributes: {
            attack: { type: Number },
            defense: { type: Number },
            healthPoints: { type: Number },
            criticalRate: { type: Number },
            criticalDamage: { type: Number },
        },
        image: { type: String }
    },
    { timestamp: true }
);

const Character = mongoose.model('Characters', characterSchema);

module.exports = Character;