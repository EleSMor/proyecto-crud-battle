const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const raceSchema = new Schema(
    {
        name: { type: String, required: true },
        raceAttributes: {
            race_attack: { type: Number, required: true },
            race_defense: { type: Number, required: true },
            race_healthPoints: { type: Number, required: true },
            race_criticalRate: { type: Number, required: true },
            race_criticalDamage: { type: Number, required: true }
        },
        characters: [{ type: mongoose.Types.ObjectId, ref: 'Characters' }],
    },
    { timestamp: true }
);

const Race = mongoose.model('Races', raceSchema);

module.exports = Race;