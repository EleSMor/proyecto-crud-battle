const mongoose = require("mongoose");
const Race = require("../models/Races.model");
const db = require("../db");

/**
 * Crear un array de datos iniciales para nuestra base de datos.
 */

const racesSeed = [
  {
    name: "Human",
    attributes: {
        race_attack: 10,
        race_defense: 20,
        race_healthPoints: 350,
        race_criticalRate: 5,
        race_criticalDamage: 10,
    },
    characters: []
  },
  {
    name: "Orc",
    attributes: {
        race_attack: 20,
        race_defense: 10,
        race_healthPoints: 250,
        race_criticalRate: 10,
        race_criticalDamage: 15,
    },
    characters: [],
  },
  {
    name: "Dwarf",
    attributes: {
        race_attack: 5,
        race_defense: 15,
        race_healthPoints: 300,
        race_criticalRate: 5,
        race_criticalDamage: 5,
    },
    characters: [],
  },
  {
    name: "Elf",
    attributes: {
        race_attack: 10,
        race_defense: 5,
        race_healthPoints: 150,
        race_criticalRate: 20,
        race_criticalDamage: 10,
    },
    characters: [],
  },
  {
    name: "Dark Elf",
    attributes: {
        race_attack: 10,
        race_defense: 5,
        race_healthPoints: 150,
        race_criticalRate: 15,
        race_criticalDamage: 40,
    },
    characters: [],
  },
];

mongoose
  .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log("Connecting to the database from the races seed...");

    const allRaces = await Race.find();

    if (allRaces.length) {

      await Race.collection.drop();
      console.log("Race collection dropped successfully...");
    }
  })
  .then(async () => {
    /**
     * Ahora, después de eliminada la colección, creo de nuevo mis mascotas iniciales.
     */

    await Race.insertMany(racesSeed);
    console.log("SUCCESS: Races added successfully...");
  })
  .catch((error) => {
    console.log("Error adding the seed", error);
  })
  .finally(() => mongoose.disconnect());
