const mongoose = require("mongoose");
const Character = require("../models/Character.model");
const db = require("../db");

/**
 * Crear un array de datos iniciales para nuestra base de datos.
 */

const characterSeed = [
  {
    name: "Astaroth",
    level: 0,
    characterClass: "Knight",
    race: "Human",
    attributes: {
        attack: 15,
        defense: 30,
        healthPoints: 500,
        criticalRate: 5,
        criticalDamage: 10,
    },
    avatarUrl: "https://static.myfigurecollection.net/pics/encyclopedia/22758.jpg",
  },
  {
    name: "Bromcram",
    level: 0,
    characterClass: "Warrior",
    race: "Orc",
    attributes: {
        attack: 30,
        defense: 15,
        healthPoints: 350,
        criticalRate: 15,
        criticalDamage: 25,
    },
    avatarUrl: "https://cdn.inprnt.com/thumbs/ff/13/ff1326193c0613c69a626d62396a3705.jpg",
  },
  {
    name: "Turnam Leadfeet",
    level: 0,
    characterClass: "Berserker",
    race: "Dwarf",
    attributes: {
        attack: 25,
        defense: 15,
        healthPoints: 300,
        criticalRate: 20,
        criticalDamage: 40,
    },
    avatarUrl: "https://www.dandwiki.com/w/images/b/ba/Dwarven_Berserker_2.jpg",
  },
  {
    name: "Eliran",
    level: 0,
    characterClass: "Wind Rider",
    race: "Elf",
    attributes: {
        attack: 20,
        defense: 15,
        healthPoints: 200,
        criticalRate: 40,
        criticalDamage: 20,
    },
    avatarUrl: "https://i.pinimg.com/originals/e2/d7/7d/e2d77d6e116b55173de0c87048df472e.jpg",
  },
  {
    name: "Valja",
    level: 0,
    characterClass: "Assassin",
    race: "Dark elf",
    attributes: {
        attack: 40,
        defense: 10,
        healthPoints: 200,
        criticalRate: 30,
        criticalDamage: 60,
    },
    avatarUrl: "https://i.pinimg.com/originals/e8/7b/67/e87b671f52201061ecf5101e1c9ce5b1.jpg",
  },
];

mongoose
  .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log("Connecting to the database from the characters seed...");

    const allCharacters = await Character.find();

    if (allCharacters.length) {

      await Character.collection.drop();
      console.log("Character collection dropped successfully...");
    }
  })
  .then(async () => {
    /**
     * Ahora, después de eliminada la colección, creo de nuevo mis mascotas iniciales.
     */

    await Character.insertMany(characterSeed);
    console.log("SUCCESS: Characters added successfully...");
  })
  .catch((error) => {
    console.log("Error adding the seed", error);
  })
  .finally(() => mongoose.disconnect());
