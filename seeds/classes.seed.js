const mongoose = require("mongoose");
const Class = require("../models/Classes.model");
const db = require("../db");

/**
 * Crear un array de datos iniciales para nuestra base de datos.
 */

const classesSeed = [
  {
    name: "Knight",
    attributes: {
      class_attack: 5,
      class_defense: 10,
      class_healthPoints: 150,
      class_criticalRate: 0,
      class_criticalDamage: 0,
    },
    characters: []
  },
  {
    name: "Warrior",
    attributes: {
      class_attack: 15,
      class_defense: 5,
      class_healthPoints: 100,
      class_criticalRate: 5,
      class_criticalDamage: 10,
    },
    characters: [],
  },
  {
    name: "Berserker",
    attributes: {
      class_attack: 20,
      class_defense: 0,
      class_healthPoints: 0,
      class_criticalRate: 15,
      class_criticalDamage: 35,
    },
    characters: [],
  },
  {
    name: "Wind Rider",
    attributes: {
      class_attack: 10,
      class_defense: 10,
      class_healthPoints: 50,
      class_criticalRate: 20,
      class_criticalDamage: 10,
    },
    characters: [],
  },
  {
    name: "Assassin",
    attributes: {
      class_attack: 40,
      class_defense: 10,
      class_healthPoints: 50,
      class_criticalRate: 15,
      class_criticalDamage: 20,
    },
    characters: [],
  },
];

mongoose
  .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(async () => {
    console.log("Connecting to the database from the seed...");

    const allClasses = await Class.find();

    if (allClasses.length) {

      await Class.collection.drop();
      console.log("Class collection dropped successfully...");
    }
  })
  .then(async () => {

    await Class.insertMany(classesSeed);
    console.log("SUCCESS: Classes added successfully...");
  })
  .catch((error) => {
    console.log("Error adding the classes seed", error);
  })
  .finally(() => mongoose.disconnect());
