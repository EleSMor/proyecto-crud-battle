const express = require('express');
const { isAdmin } = require('../middlewares/auth.middleware');

const {
   classesGet,
   createClassGet,
   createClassPost,
} = require('../controllers/classes.controllers')

const router = express.Router();

router.get('/', isAdmin, classesGet);

router.get('/create', isAdmin, createClassGet);
router.post('/create', createClassPost);

// router.get('/edit/:id', isAdmin, )
// router.put('/edit/:id', isAdmin,  );

// router.delete('/delete/:id', isAdmin, );

module.exports = router;