const express = require('express');
const { isAdmin } = require('../middlewares/auth.middleware');
const { upload, uploadToCloudinary } = require("../middlewares/file.middleware");

const {
    charactersGet,
    createCharGet,
    createCharPost,
    editCharGet,
    editCharPut,
    deleteCharacter,
} = require('../controllers/characters.controllers')

const router = express.Router();

router.get('/', isAdmin, charactersGet);

router.get('/create', isAdmin, createCharGet)
router.post('/create', [isAdmin, upload.single('image'), uploadToCloudinary], createCharPost)

router.get('/edit/:id', isAdmin, editCharGet)
router.put('/edit/:id', [isAdmin, upload.single('image'), uploadToCloudinary], editCharPut);

router.delete('/delete/:id', isAdmin, deleteCharacter);

module.exports = router;