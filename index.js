const express = require('express');
const dotenv = require('dotenv');
dotenv.config();
const path = require('path');
const methodOverride = require('method-override');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const passport = require('passport');
require('./auth');

const db = require('./db');
const indexRoutes = require('./routes/index.routes');
const authRoutes = require('./routes/auth.routes');
const charactersRoutes = require('./routes/characters.routes');
const classesRoutes = require('./routes/classes.routes');

db.connect();

const PORT = process.env.PORT || 3000;

const app = express();

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1 * 24 * 3600 * 1000,
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(methodOverride('_method'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use((req, res, next) => {
    req.isAdmin = false;

    if (!req.isAuthenticated()) {
        return next();
    } else {
        req.isUser = true;
    }

    if (req.user && req.user.role == 'admin') {
        req.isAdmin = true;
    }

    return next();
})

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/characters', charactersRoutes);
app.use('/classes', classesRoutes);
// app.use('/races', racesRoutes);

app.use('*', (req, res) => {
    const error = new Error('Path unfinded');
    error.status = 404;

    return res.status(404).json(error);
})

app.use((error, req, res, next) => {
    return res.status(error.status || 500).render('error', {
        message: error.message || 'Unexpected error, try again',
        status: error.status || 500,
    });
});

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});