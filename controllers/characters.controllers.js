const Character = require('../models/Character.model');
const Race = require('../models/Races.model');
const Class = require('../models/Classes.model');

const charactersGet = async (req, res, next) => {
    try {
        const characters = await Character.find();
        return res.render('characters', { characters, isAdmin: req.isAdmin, user: req.user });
    } catch (error) {
        return next(error);
    }
};

const createCharGet = async (req, res, next) => {
    const classes = await Class.find();
    const races = await Race.find();
    return res.render('character-create', { classes, races, user: req.user });
};

const createCharPost = async (req, res, next) => {
    try {
        const { name, characterClass, level, race } = req.body;

        const image = req.fileUrl ? req.fileUrl : '';
        const [{ classAttributes }] = await Class.find({ name: characterClass });
        const [{ raceAttributes }] = await Race.find({ name: race });

        const {
            class_attack,
            class_defense,
            class_healthPoints,
            class_criticalRate,
            class_criticalDamage
        } = classAttributes

        const {
            race_attack,
            race_defense,
            race_healthPoints,
            race_criticalRate,
            race_criticalDamage
        } = raceAttributes

        const attributes = {
            attack: class_attack + race_attack,
            defense: class_defense + race_defense,
            healthPoints: class_healthPoints + race_healthPoints,
            criticalRate: class_criticalRate + race_criticalRate,
            criticalDamage: class_criticalDamage + race_criticalDamage,
        }

        console.log(attributes);

        const newCharacter = new Character({ name, characterClass, level, race, attributes, image });
        const createdCharacter = await newCharacter.save();

        return res.redirect('/characters');

    } catch (error) {
        return next(error);
    }
};

const editCharGet = async (req, res, next) => {
    const { id } = req.params;

    try {
        const character = await Character.findById(id);
        const classes = await Class.find();
        const races = await Race.find();
        return res.render('character-edit', { character, classes, races });
    } catch (error) {
        return next(error)
    }
};

const editCharPut = async (req, res, next) => {
    try {
        const { id } = req.params;
        console.log(req.body);
        const { name, characterClass, level, race, image } = req.body;
        const fieldsToUpdate = {};

        if (name) fieldsToUpdate.name = name;
        if (characterClass) fieldsToUpdate.characterClass = characterClass;
        if (level) fieldsToUpdate.level = level;
        if (race) fieldsToUpdate.race = race;
        if (characterClass || race) {
            const [{ classAttributes }] = await Class.find({ name: characterClass });
            const [{ raceAttributes }] = await Race.find({ name: race });

            const {
                class_attack,
                class_defense,
                class_healthPoints,
                class_criticalRate,
                class_criticalDamage
            } = classAttributes

            const {
                race_attack,
                race_defense,
                race_healthPoints,
                race_criticalRate,
                race_criticalDamage
            } = raceAttributes

            fieldsToUpdate.attributes = {
                attack: class_attack + race_attack,
                defense: class_defense + race_defense,
                healthPoints: class_healthPoints + race_healthPoints,
                criticalRate: class_criticalRate + race_criticalRate,
                criticalDamage: class_criticalDamage + race_criticalDamage,
            }

            console.log(fieldsToUpdate.attributes);
        }
        if (image) fieldsToUpdate.image = req.fileUrl ? req.fileUrl : '';


        const updatedCharacter = await Character.findByIdAndUpdate(
            id,
            fieldsToUpdate,
            { new: true }
        );

        return res.redirect('/characters');
    } catch (error) {
        return next(error);
    }
};

const deleteCharacter = async (req, res, next) => {
    try {
        const { id } = req.params;

        const deleted = await Character.findByIdAndDelete(id);

        if (deleted) {
            const characters = await Character.find();
            return res.render('characters', { characters, deleted: true });
        } else {
            const error = new Error("Can't find a character with this id. Try again");
            error.status = 400;
            return res.render('error', { message: error.message, status: error.status });
        }

    } catch (error) {
        return next(error);
    }
};

module.exports = {
    charactersGet,
    createCharGet,
    createCharPost,
    editCharGet,
    editCharPut,
    deleteCharacter
}