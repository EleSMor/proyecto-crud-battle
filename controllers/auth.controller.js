const passport = require('passport');

const registerGet = (req, res, next) => {
    return res.render('registerview');
}

const registerPost = (req, res, next) => {
    const { email, username, name, password } = req.body;

    if (!email || !username || !name || !password) {
        const error = new Error('Complete fields');
        return res.render('registerview', { error });
    }

    const done = (error, user) => {

        if (error) {
            return next(error);
        };

        req.logIn(user, (error) => {
            if (error) {
                return next(error);
            };
            return res.redirect('/');
        });
    };

    passport.authenticate('register', done)(req);
};

const loginGet = (req, res, next) => {
    return res.render('loginview');
};

const loginPost = (req, res, next) => {
    const done = (error, user) => {
        if (error) return next(error);

        const doneForSerialize = (error, user) => {
            if (error) return next(error);
            return res.redirect('/');
        };
        req.logIn(user, doneForSerialize);
    };
    passport.authenticate('login', done)(req);
};

const logoutPost = (req, res, next) => {
    if (req.user) {
        req.logout();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.redirect('/');
        });
    } else {
        return res.status(200).json("No user logged in")
    };
};

module.exports = {
    registerGet,
    registerPost,
    loginGet,
    loginPost,
    logoutPost,
}