const indexController = (req, res, next) => {
    return res.render('index', {
        title: 'Welcome to CRUD Battle',
        user: req.user,
    });
};

module.exports = {
    indexController,
}