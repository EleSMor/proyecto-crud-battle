const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');

/**************************************
 * Login Strategy
 *************************************/

 const loginStrategy = new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            
            let existingUser = await User.findOne({ email });
            
            if (!existingUser) {
                const error = new Error("Email or Username doesn't exist");
                error.status = 401;
                return done(error, null);
            }

            const isValidPassword = await bcrypt.compare(password, existingUser.password);

            if (!isValidPassword) {
                const error = new Error("Incorrect password. Try again");
                return done(error, null);
            }

            existingUser.password = null;
            return done(null, existingUser);

        } catch (error) {
            return done(error, null);
        }
    },
);

module.exports = loginStrategy;