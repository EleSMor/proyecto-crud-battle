const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User.model');
const { isValidPassword, isValidEmail } = require('./utils');

/**************************************
 * Register Strategy
 *************************************/

const registerStrategy = new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },

    async (req, email, password, done) => {
        try {
            const existingUser = await User.findOne({ email });

            if (existingUser) {
                const error = new Error("Email is already registered")
                error.status = 400;
                return done(error);
            }

            if (!isValidEmail(email)) {
                const error = new Error('Invalid email format');
                error.status = 400;
                return done(error);
            }

            if (!isValidPassword(password)) {
                const error = new Error('Invalid password format')
                error.status = 400;
                return done(error);
            }

            const saltRounds = 10;
            const passwordHash = await bcrypt.hash(password, saltRounds);

            const newUser = new User({
                email,
                password: passwordHash,
                username: req.body.username,
                name: req.body.name,
                lastName: req.body.lastName,
            });

            const savedUser = await newUser.save();
            savedUser.password = null;

            return done(null, savedUser);

        } catch (error) {
            return done(error);
        }
    },
);


module.exports = registerStrategy;